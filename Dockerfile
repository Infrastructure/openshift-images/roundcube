FROM docker.io/roundcube/roundcubemail:latest

COPY custom/roundcube-override.ini /usr/local/etc/php/conf.d/roundcube-override.ini 

RUN sed -ri -e 's/:80/:8000/g' /etc/apache2/sites-available/000-default.conf && \
    sed -ri -e 's/80/8000/g' /etc/apache2/ports.conf && \
    sed -ri -e 's/DEFAULT@SECLEVEL=2/DEFAULT@SECLEVEL=1/' /etc/ssl/openssl.cnf
